# Emergya Drupal Landing Page Test Project

This project creates a new Drupal 8 site with a Landing page that contains a slider.

This project provides a site that manages its dependencies with [Composer](https://getcomposer.org/).

## Usage

First you need to [install DDEV](https://ddev.com/get-started/).

## Starting Project

Follow the steps below to start the project using DDEV.

1. Run `git clone https://gitlab.com/Riloto/d8landingpage.git` to clone the code of the repository.
2. Run `ddev start` to start the ddev project.
3. Import database file placed in root directory: Run `ddev import-db emergya_landing.sql.gz`.
4. Run `ddev drush cr` to clear all caches.
5. Run `ddev drush uli` to obtain a one-usage url to access as admin.


Authored by: **Ricardo López Toro**
