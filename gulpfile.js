const gulp = require('gulp');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const sourcemaps = require('gulp-sourcemaps');
const importer = require('node-sass-globbing');
const autoprefixer = require('gulp-autoprefixer');
const stripCssComments = require('gulp-strip-css-comments');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const livereload = require('gulp-livereload');

let sass_config = {
  importer: importer,
  includePaths: [
    'node_modules/breakpoint-sass/stylesheets/',
    'node_modules/singularitygs/stylesheets/',
    'node_modules/modularscale-sass/stylesheets',
    'node_modules/compass-mixins/lib/'
  ]
};

// Gulp plumber error handler
let onError = function(err) {
  console.log(err);
}


function sassThemes(){
  return gulp.src('web/themes/custom/**/sass/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass(sass_config).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(stripCssComments({preserve: false}))
    .pipe(cssmin())
    .pipe(sourcemaps.write('.'))
    .pipe(rename(function (path) {
      path.dirname = path.dirname.replace('/sass', '/css');
    }))
    .pipe(gulp.dest('web/themes/custom/'));
}

function sassModules(){
  return gulp.src('web/modules/custom/**/sass/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass(sass_config).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(stripCssComments({preserve: false}))
    .pipe(cssmin())
    .pipe(sourcemaps.write('.'))
    .pipe(rename(function (path) {
      path.dirname = path.dirname.replace('/sass', '/css');
    }))
    .pipe(gulp.dest('web/modules/custom/'));
}

function watch(){
  // Whenever a stylesheet is changed, recompile
  gulp.watch('web/themes/custom/**/sass/**', {usePolling: true}, sassThemes);
  gulp.watch('web/modules/custom/**/sass/**', {usePolling: true}, sassModules);

  // Create a LiveReload server
  livereload.listen();

  // Watch any file for a change and reload as required
  gulp.watch([
    'web/themes/custom/**/css/style.css',
    'web/modules/custom/**/css/style.css',
  ], {usePolling: true}).on('change', function(files) {
    livereload.changed(files);
  })
}

var compile = gulp.parallel(sassThemes, sassModules);
exports.compile = compile;
exports.default = gulp.series(compile, watch);
